<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    public function details()
    {
        return $this->hasMany('App\FilterDetail','filter_id');
    }
}
