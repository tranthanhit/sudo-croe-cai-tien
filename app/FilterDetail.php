<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterDetail extends Model
{
    public $incrementing = false;
    public $timestamp = false;

}
