<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use Illuminate\Support\Facades\Auth;

use App\Comment;
use App\Product;
use App\News;

class CommentController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Đánh giá';
        $this->table_name = 'comments';
        parent::__construct();
    }
    public function index(Request $request)
    {
        // $d = file_get_contents('https://noithathoathinh.com/api/get-products');

        // $list = json_decode($d);
        // foreach ($list->review_conment_user_tuanbsc as $key => $value) {
           
        //    $create_product = Comment::create([
        //         'id' =>$value->id,
        //         'parent_id'=>$value->parent_id,
        //         'type'=>$value->type,
        //         'type_id'=>$value->type_id,
        //         'name'=>$value->name,
        //         'email'=>$value->email,
        //         'phone'=>$value->phone,
        //         'like'=>0,
        //         'rank'=>5,
        //         'content'=>$value->content,
        //         'status'=>$value->status,
        //         'created_at'=>$value->created_at,
        //         'updated_at'=>$value->updated_at,
        //    ]);
        // }

        // $products = Comment::where('status',4)->delete();
        // $products1 = Comment::where('status',3)->delete();
        // $products2 = Comment::where('status',2)->delete();
        // $products3= Comment::where('status',1)->delete();

        $this->checkRole($this->table_name.'_access');
        $arr_stt = [1=>'Đã duyệt',2=>'Chờ duyệt',3=>'Hủy'];

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('status','Trạng thái','array',1,$arr_stt);
        $listdata->add('content','Nội dung bình luận','string',1);
        $listdata->add('phone','Thông tin khách','string',1);
        $listdata->add('created_at','Thông tin','range',1);
        $listdata->add('','Hành động');
        // $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        // $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();



        $comment_data = $data['show_data'];

        // dump($comment_data);
        $comment_adm = $comment_data->pluck('id');
        $comment_products = $comment_news = [];
        foreach ($comment_data as $value) {
            switch ($value->type) {
                case 'products':
                    $comment_products[] = $value->type_id;
                break;
                case 'news':
                    $comment_news[] = $value->type_id;
                break;
            }
        }

        $products = Product::whereIn('id',$comment_products)->get();

        $news = News::whereIn('id',$comment_news)->get();
        
        $comment_admins = Comment::whereIn('parent_id',$comment_adm)
            ->leftJoin('admin_users','admin_users.id','comments.admin_id')
            ->select('comments.*','admin_users.name')
            ->get();
        //dd($comment_admins);
        return view('admin.layouts.list',compact('data','arr_stt','comment_admins','products','news'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update(['status'=>4]);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }
    }
    public function comment_success(Request $request){
        $id= $request->id;
        $val= $request->val;
        $comment = Comment::where('status','<>',4)->where('id',$id)->first();
        $comment->status = $val;
        $comment->save();
        return response(json_encode(compact('val')));
    }
    public function reply($id) {
        $this->checkRole($this->table_name.'_create');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        if($data_edit->parent_id == 0) {
            $data_parent = $data_edit;
        }else {
            $data_parent = DB::table($this->table_name)->where('id',$data_edit->parent_id)->first();
        }
        $child = DB::table($this->table_name)->where('parent_id',$data_parent->id)->get();
        $data = [
            'comment' => $data_parent,
            'child' => $child
        ];
        return view('admin.comments.reply',compact('data','id'));
    }
    public function postReply(Request $request, $id) {

        $this->checkRole($this->table_name.'_create');

        $this->validate_form($request,'name',1,'Bạn chưa nhập tên');
        $this->validate_form($request,'content',1,'Bạn chưa nhập nội dung');

        $parent_id = $id;
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();
        $adm = Auth::guard('admin')->user();
        $admin_id = $adm->id;
        $name = $request->get('name');
        $content = $request->get('content');
        $status = 1;
        $type = 'products';
        $created_at = $updated_at = date('Y-m-d H:i:s');

        $insert_id = DB::table('comments')->insertGetId(compact('parent_id','name','admin_id','content','status','type','created_at','updated_at'));
        
        return redirect()->back();
        // return redirect(route($this->table_name.'.edit',$insert_id))->with(['flash_level'=>'success','flash_message'=>'Thêm trả lời thành công!']);
    }

    public function quick_comment(Request $request) {
        $id = $request->id;
        $name = $request->name;
        $content = $request->content;
        $created_at = $updated_at = date('Y-m-d H:i:s');
        $data = [
            'name'          => $name,
            'type'          => null,
            'type_id'       => null,
            'parent_id'     => $id,
            'admin_id'      => Auth::guard('admin')->user()->id,
            'content'       => $content,
            'email'         => null,
            'phone'         => null,
            'like'          => 0,
            'status'        => 1,
            'gender'        => 1,
            'rank'          => 5,
            'created_at'    => $created_at,
            'updated_at'    => $updated_at,
        ];
        $insert_id = DB::table('comments')->insertGetId($data);
        return response()->json(['name'=>Auth::guard('admin')->user()->name??'Admin vô danh','content'=>$content,'parent_id'=>$id]);
    }
}
