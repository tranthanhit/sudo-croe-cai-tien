<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use DB;
// use App\Exports\OrdersExport;
// use Maatwebsite\Excel\Facades\Excel;
class SupportController extends Controller
{
    function __construct()
    {
        $this->module_name = 'Support';
        $this->table_name = 'supports';
        parent::__construct();
    }
    public function index(Request $request)
    {
        $arr_stt = [0=>'Đơn hàng mới',1=>'Đã tiếp nhận',2=>'Hoàn thành',3=>'Hủy'];
        $arr_type = [
            'call_me' => "Gọi ngay",
            'services' => "Đặt lịch sửa chữa",
            'fits' => "Mua phụ kiện",
            'contact' => 'Liên hệ',
            'promotion_notice' => 'Thông báo khuyến mãi',
        ];
        $this->checkRole($this->table_name.'_access');

        // $location_array = DB::table('location')->where('status',1)->orderBy('id','asc')->pluck('name','id')->toArray();
     

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('status','Trạng thái','array',1,$arr_stt);
        $listdata->add('name','Họ tên','string',0);
        $listdata->add('phone','Số điện thoại','string',0);
        $listdata->add('email','Email','string',0);
        $listdata->add('address','Thông tin khác','string');
        // $listdata->add('location_id','Địa điểm','array',1,$location_array);
        $listdata->add('type','Kiểu','array',1,$arr_type);
        $listdata->add('note','Ghi chú','string');
        $listdata->add('created_at','Thời gian','range',1);
        $listdata->add('','Chi tiết');
        // $listdata->add('status','Trạng thái','status',1,[1=>'Đơn hàng mới',2=>'Đã tiếp nhận',3=>'Hoàn thành']);
        // $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();

        return view('admin.layouts.list',compact('data','arr_stt','arr_type'));
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        
    }
}
