<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MyClass\MyForm;
use App\MyClass\ListData;
use App\MyClass\Categories;
use DB;
use App\Trademark;

class TrademarkController extends Controller
{
    function __construct()
    {
        $this->module_name = 'thương hiệu';
        $this->table_name = 'trademark';
        $this->has_seo = false;
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      

        $this->checkRole($this->table_name.'_access');

        $listdata = new ListData($request,$this->table_name);
        $listdata->add('name','Tên thương hiệu','string',1);
        $listdata->add('image','Ảnh đại diện','string');
        $listdata->add('','Thông tin');
        $listdata->add('status','Trạng thái','status',1,[1=>'Hoạt động',2=>'Không hoạt động',3=>'Thùng rác']);
        $listdata->add('','Sửa','edit');
        $listdata->add('','Xóa','delete');

        $data = $listdata->data();
        return view('admin.layouts.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->checkRole($this->table_name.'_create');

        $form = new MyForm();
        $data_form[] = $form->text('name','',1,'Tên thương hiệu','',1,'slug');
        $data_form[] = $form->slug('slug','');
        $data_form[] = $form->image('image','',0);
        $data_form[] = $form->checkbox('status',1,1,'Kích hoạt');
        $data_form[] = $form->action('add');
        return view('admin.layouts.create',compact('data_form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->checkRole($this->table_name.'_create');
        $this->validate_form($request,'name',1,'Bạn chưa nhập tên');
        
        $created_at = $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        //Kiểm tra xem slug đã tồn tại và có status = 4 trong DB chưa. nếu tồn tại thì xóa đi
        $this->checkSlug($this->table_name, $data_form['slug']);
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);// đưa mảng về các biến có tên là các key của mảng
        $data_insert = compact('name','image','slug','status','created_at','updated_at');
        $id_insert = DB::table($this->table_name)->insertGetId($data_insert);

        $this->systemLogs('Thêm mới '.$this->module_name,'store',$this->table_name,$id_insert,$data_insert);
        return redirect(route($this->table_name.'.'.$redirect,$id_insert))->with(['flash_level'=>'success','flash_message'=>'Thêm mới thành công!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->checkRole($this->table_name.'_edit');

        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $form = new MyForm();
        $data_form[] = $form->text('name',$data_edit->name,1,'Tên thương hiệu');
        $data_form[] = $form->slug('slug',$data_edit->slug,1,'Đường dẫn thương hiệu');
        $data_form[] = $form->image('image',$data_edit->image,1);
        $data_form[] = $form->checkbox('status',$data_edit->status,1,'Kích hoạt');
        $data_form[] = $form->action('edit');
        return view('admin.layouts.edit',compact('data_form','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->checkRole($this->table_name.'_edit');
        $data_edit = DB::table($this->table_name)->where('id',$id)->first();

        $this->validate_form($request,'name',1,'Bạn chưa nhập tên');

        $updated_at = date("Y-m-d H:i:s");
        $data_form = $request->all();
        $status = 2;
        extract($data_form,EXTR_OVERWRITE);
        $data_update = compact('name','status','slug','image','updated_at');

        DB::table($this->table_name)->where('id',$id)->update($data_update);
        
        $old = [
            'name'=>$data_edit->name,
            'image'=>$data_edit->image,
            'slug'=>$data_edit->slug,
            'status'=>$data_edit->status,
            'updated_at'=>$data_edit->updated_at
        ];
        $this->systemLogs('Sửa '.$this->module_name,'update',$this->table_name,$id,['old'=>$old,'new'=>$data_update]);

        return redirect(route($this->table_name.'.'.$redirect,$id))->with(['flash_level'=>'success','flash_message'=>'Cập nhật dữ liệu thành công!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->hasRole($this->table_name.'_delete')) {
            DB::table($this->table_name)->where('id',$id)->update([
                'status'=>4,
                'name' => DB::raw("CONCAT(name, '--delete--".time()."')")
            ]);
            $this->systemLogs('Xóa '.$this->module_name,'delete',$this->table_name,$id);
            return response()->json(['status'=>1,'message'=>'Xóa thành công']);
        }else {
            return response()->json(['status'=>0,'message'=>'Xóa không thành công']);
        }

    }
}
