<?php
namespace App\Http\Controllers\Web;
use Illuminate\Http\Request;
use Cart;
use Cache;
use DB;
use Illuminate\Support\Facades\Auth;
class AjaxController extends Controller
{
    //thêm vào giỏi hàng
    public function Order(Request $request)
    {
        if($request->ajax()){
            extract($request->all(),EXTR_OVERWRITE);
            $data = DB::table($type)->where('id',$id)->first();
          
            $name = $data->name;
            $price = $data->price??0;
            $image = $data->image;
            $cart_info = [
                "id" => $id,
                "name" => $name,
                "price" => $price,
                "qty" => $qty,
                "options" => [
                    'type' => $type,
                    'image' => $image,
                    'slug' => $data->slug,
                ],
                
            ];
            Cart::add($cart_info);
            $cart_content = Cart::content();
            $cart_count = Cart::count();
            $cart_total = Cart::total();
            return response(json_encode(compact('cart_content','cart_count','cart_total','qty','image','name','price')));
        }
    }

     /**
     * đăng nhập
     */
    public function AjaxLogin(Request $request)
    {
        if ($request->ajax()) {
            $email = $request->email_login;
            $password = $request->password_login;
            if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {
                return 1;
            } else {
                if (Auth::attempt(['name' => $email, 'password' => $password, 'status' => 1])) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }
    /**
     * đăng ký
     */
    public function AjaxRegister(Request $request)
    {
      
        if ($request->ajax()) {
            $name = $request->name_registration;
            $surname = $request->surname_registration;
            $email = $request->email_registration;
            $password = $request->password_registration;
            $check_email = DB::table('users')->where('email', $email)->where('status', '!=', 4)->first();
            $check_name = DB::table('users')->where('name', $name)->where('status', '!=', 4)->first();
            $today = date("Y-m-d H:i:s");
            $data['email'] = $email;
            $data['name'] = $name;

            $result['status'] = 0;
            $result['message'] = "";
            if (!empty($check_name)) {
                $result['message'] = "Tên đăng nhập đã được sử dụng";
                $result['status'] = -1;
            } elseif (!empty($check_email)) {
                $result['message'] = "Email đã được sử dụng";
                $result['status'] = 0;
            } else {
                $check = DB::table('users')->where('email', $email)->where('status', 4)->first();
                if (!empty($check)) {
                    DB::table('users')->where('id', $check->id)->delete();
                }
                $db_insert = [
                    'name' => $name,
                    'surname'=>$surname,
                    'email' => $email,
                    'password' => bcrypt($password),
                    'created_at' => $today,
                    'updated_at' => $today,
                    'image' => '',
                    'fullname' => '',
                    'phone' => '',
                    'status' => 2,
                ];
                $id_insert = DB::table('users')->insertGetId($db_insert);
                // $data['id'] = $id_insert;
                // $data['password'] = $password;

                // Mail::to($email)->send(new comfirmEmail($data));
                $result['status'] = 1;
            }
            return json_encode($result);
        }
    }
}
