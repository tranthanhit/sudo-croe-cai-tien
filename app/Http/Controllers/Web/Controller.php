<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

use DB;
use View;
use Illuminate\Support\Facades\Cache;
class Controller extends BaseController
{
    public function __construct()
    {
        //constructor cho web
        $variable = 'variable';
        View::share('variable',$variable);

        if(cache::has('config_general')) {
            $config_general = Cache::get('config_general');
        }else{
            $config_general = DB::table('options')->select('value')->where('name','general')->first();
            $config_general = json_decode(base64_decode($config_general->value),true);
            Cache::forever('config_general', $config_general);
        }

        if(cache::has('menu_primary_top')) {
            $menu_primary_top = Cache::get('menu_primary_top');
        }else {
            $menu_primary_top = $this->ChildrenCategoryOption('general', 'menu_primary');
            Cache::forever('menu_primary_top', $menu_primary_top);
             
        }
       

        if(cache::has('menu_footer')) {
            $menu_footer = Cache::get('menu_footer');
        }else {
            $menu_footer = $this->ChildrenCategoryOption('general', 'menu_footer');
            Cache::forever('menu_footer', $menu_footer);
             
        }


        View::share('config_general',$config_general);

        View::share('menu_primary_top', $menu_primary_top);
        View::share('menu_footer', $menu_footer);

    }

    /**
     * @param string $type - tên kiểu trong bảng meta_seo
     * @param int $id - id trong bảng meta_seo
     * @param array $options - các trường mặc định hoặc không có trong bảng meta_seo: title | description | robots | type | url | image |
     */
    public function meta_seo($type='',$id=0,$options) {
        $meta_seo = [];
        if(count($options)) {
            foreach ($options as $key=>$value) {
                $meta_seo[$key] = $value;
            }
        }
        if ($type != '' && $id != 0) {
            $data_seo = DB::table('meta_seo')->where('type',$type)->where('type_id',$id)->first();
            if($data_seo) {
                if($data_seo->title!=''){
                    $meta_seo['title'] = $data_seo->title;
                }
                if($data_seo->description!=''){
                    $meta_seo['description'] = $data_seo->description;
                }
                $meta_seo['robots'] = $data_seo->robots;
            }
        }
        return $meta_seo;
    }
    public function ChildrenCategoryOption($setting, $data)
    {
        $category = DB::table('options')->select('value')->where('name', $setting)->first();
        $category = json_decode(base64_decode($category->value), true);
        $data = json_decode($category[$data]);
        foreach ($data as $value) {
            if (!empty($value->children)) {
                $children = $value->children;
                $value->children = $children;
            }
        }
        return $data;
    }
}
