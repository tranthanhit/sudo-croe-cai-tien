<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Product;
use App\ProductCategory;

class ProductController extends Controller
{
    public function AllShow()
    {
        $data = Product::where('status',1)->paginate(20);
        $product_categories = ProductCategory::where('status',1)->get();
        $meta_seo = $this->meta_seo('',0,
        [
            'title' => 'Sản phẩm',
            'description'=> 'Sản phẩm',
            'url' => url('').'/san-pham',
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],         
        ];
        return view('web.products.index',compact('data','meta_seo','breadcrumbs','product_categories'));
    }
    public function index($slug)
    { 
        $category = ProductCategory::where('status',1)->where('slug',$slug)->firstOrFail();
        $product_categories = ProductCategory::where('status',1)->get();
        $data = Product::where('status',1)->where('category_id',$category->id)->paginate(20); 
        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $category->getName(),
            'description'=> $category->getDes(160),
            'url' => $category->getUrl(),
            'image'=> $category->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],   
            ['name'=> $category->getName(),'url' =>  $category->getUrl()],         
        ];

        return view('web.products.index',compact('data','meta_seo','breadcrumbs','product_categories'));
    }
    public function show($slug)
    {
        $product = Product::where('status',1)->where('slug',$slug)->firstOrFail();

        if (isset($product->slides) && $product->slides != '') {
            $slides = explode(",",$product->slides);
        }else {
            $slides = '';
        }

        $product_category = ProductCategory::where('status',1)->where('id',$product->category_id)->firstOrFail();

        
        
        if ($product->related_products != '') {
            //sản phẩm liên quan
            $related_product = $product->get_related_product();
        }

        // Nếu không có sp lq được chọn lấy auto 5 sp cùng danh mục
        if ($product->related_products == '') {
            $related_product = Product::where('status',1)->where('category_id',$product->category_id)->paginate(5);
        }

        $meta_seo = $this->meta_seo('',0,
        [
            'title' => $product->getName(),
            'description'=> $product->getDes(160),
            'url' => $product->getUrl(),
            'image'=> $product->getImage(),
        ]);
        $breadcrumbs = [
            ['name'=> 'Sản phẩm','url' => '/san-pham'],   
            ['name'=> $product_category->getName(),'url' =>  $product_category->getUrl()],         
        ];

        return view('web.products.show',compact('product','meta_seo','breadcrumbs','product_category','related_product','slides'));
    }
}
