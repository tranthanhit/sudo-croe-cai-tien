<?php

namespace App\MyClass;

use DB;

class ListData
{
    private $serial = 0;
    private $table_name;
    private $id_name;
    private $field = [];
    private $label = [];
    private $type = [];
    private $search = [];
    private $search_option;
    private $search_option_value;
    private $total_record;
    private $page = 1;
    private $page_size;
    private $prefix_link;

    public function __construct($request,$table_name = '', $id_name = 'id', $page_size = 30){
        $this->request = $request;
        $this->table_name = $table_name;
        $this->id_name = $id_name;
        $this->page_size = $page_size;

        $this->prefix_link = '/admin/'.$table_name.'?s';
    }

    function add($field_name,$label,$type = 'string',$search = 0, $search_option = ''){
        $this->field[$this->serial] = $field_name;
        $this->label[$this->serial] = $label;
        $this->type[$this->serial] = $type;
        if($search) $this->search[$this->serial] = $field_name;
        if($search) $this->search_option[$this->serial] = $search_option;
        $this->serial++;
    }

    function data() {
        $show_data = DB::table($this->table_name)->where('status','!=',4);
        $total_record = DB::table($this->table_name)->where('status','!=',4);
        if($this->table_name == 'admin_users') {//ẩn user dev khỏi danh sách admin
            $show_data = $show_data->where('name','<>','dev');
            $total_record = $total_record->where('name','<>','dev');
        }
        if(isset($this->request->search)) {
            $this->prefix_link .= '&search=1';
            foreach ($this->search as $key=>$field) {
                $search_value = $this->request->$field;

                switch ($this->type[$key]) {
                    case 'string' :
                        if ($search_value != null && trim($search_value) != '') {
                            $search_string = str_replace(" ","%",$search_value);
                            $show_data = $show_data->where($field,'LIKE','%'.$search_string.'%');
                            $total_record = $total_record->where($field,'LIKE','%'.$search_string.'%');
                        }
                        $this->search_option_value[$field] = $search_value;
                        $this->prefix_link .= '&'.$field.'='.$search_value;
                        break;
                    case 'array' :
                        if($search_value != null && $search_value != -1) {
                            $show_data = $show_data->where($field,$search_value);
                            $total_record = $total_record->where($field,$search_value);
                        }else {
                            $search_value = -1;
                        }
                        $this->search_option_value[$field] = $search_value;
                        $this->prefix_link .= '&'.$field.'='.$search_value;
                        break;
                    case 'status' :
                        if($search_value != null && intval($search_value)> -1) {
                            $show_data = $show_data->where($field,intval($search_value));
                            $total_record = $total_record->where($field,intval($search_value));
                        }else {
                            $search_value = -1;
                        }
                        $this->search_option_value[$field] = $search_value;
                        $this->prefix_link .= '&'.$field.'='.$search_value;
                        break;
                    case 'logs':
                        if ($search_value != -1) {
                            $show_data = $show_data->where($field,'LIKE',$search_value);
                            $total_record = $total_record->where($field,'LIKE','%'.$search_value.'%');
                        }
                        $this->search_option_value[$field] = $search_value;
                        $this->prefix_link .= '&'.$field.'='.$search_value;
                        break;
                    case 'range':
                        $field_start = $field.'_start';
                        $field_end = $field.'_end';
                        $search_value_start = $this->request->$field_start;
                        $search_value_end = $this->request->$field_end;
                        if ($search_value_start != null && $search_value_end != null) {
                            $show_data = $show_data->where($field,'>',$search_value_start)->where($field,'<',$search_value_end);
                            $total_record = $total_record->where($field,'>',$search_value_start)->where($field,'<',$search_value_end);
                        }
                        $this->search_option_value[$field] = ['start'=>$search_value_start,'end'=>$search_value_end];
                        $this->prefix_link .= '&'.$field_start.'='.$search_value_start;
                        $this->prefix_link .= '&'.$field_end.'='.$search_value_end;
                        break;
                }
            }
        }
        $show_data = $show_data->orderBy($this->id_name,'DESC');
        $this->prefix_link .= '&page=';
        if(isset($this->request->page)) {
            $this->page = intval($this->request->page);
        }
        $page_start = ($this->page - 1) * $this->page_size;
        $show_data = $show_data->limit($this->page_size)->offset($page_start)->get();
        $total_record = $total_record->count();
        $this->total_record = $total_record;

        return [
            'table' => $this->table_name,
            'show_data' => $show_data,
            'id_name' => $this->id_name,
            'field' => $this->field,
            'label' => $this->label,
            'type' => $this->type,
            'search' => $this->search,
            'search_option' => $this->search_option,
            'search_option_value' => $this->search_option_value,
            'total_record' => $this->total_record,
            'page' => $this->page,
            'page_size' => $this->page_size,
            'prefix_link' => $this->prefix_link
        ];
    }

    private $categories_data = [];
    private $categories_index = -1;
    function data_categories($parent_id = 0,$level = 0,$status = [1]) {
        $list_categories = DB::table($this->table_name)
            ->whereIn('status',$status)->where('parent_id',$parent_id);
        if(isset($this->request->search)) {
            $this->prefix_link .= '&search=1';
            foreach ($this->search as $key=>$field) {
                $search_value = $this->request->$field;

                switch ($this->type[$key]) {
                    case 'string' :
                        if ($search_value != null && trim($search_value) != '') {
                            $list_categories = $list_categories->where($field,'LIKE','%'.$search_value.'%');
                        }
                        $this->search_option_value[$field] = $search_value;
                        break;
                    case 'array' :
                        if($search_value != null && intval($search_value)> -1) {
                            $list_categories = $list_categories->where($field,intval($search_value));
                        }else {
                            $search_value = -1;
                        }
                        $this->search_option_value[$field] = $search_value;
                        break;
                    case 'status' :
                        if($search_value != null && intval($search_value)> -1) {
                            $list_categories = $list_categories->where($field,intval($search_value));
                        }else {
                            $search_value = -1;
                        }
                        $this->search_option_value[$field] = $search_value;
                        break;
                }

                $this->prefix_link .= '&'.$field.'='.$search_value;
            }
        }
        $list_categories = $list_categories->orderBy('order','ASC')->get();

        if($list_categories->count()) {
            $num = 0;
            foreach ($list_categories as $key => $value) {
                $num++;
                $this->categories_index++;

                $value->level = $level;
                if($num < $list_categories->count()){
                    $value->last = 0;
                }
                else{
                    $value->last = 1;
                }

                $list_child_categories = DB::table($this->table_name)
                    ->whereIn('status',$status)->where('parent_id',$value->id)
                    ->orderBy('order','ASC')
                    ->get();
                if ($list_child_categories->count()) {
                    $value->haschild = 1;
                    $this->categories_data[$this->categories_index] = $value;
                    $this->data_categories($value->id,$level+1,$status);
                }else {
                    $value->haschild = 0;
                    $this->categories_data[$this->categories_index] = $value;
                }
            }
        }
        return [
            'table' => $this->table_name,
            'show_data' => $this->categories_data,
            'id_name' => $this->id_name,
            'field' => $this->field,
            'label' => $this->label,
            'type' => $this->type,
            'search' => $this->search,
            'search_option' => $this->search_option,
            'search_option_value' => $this->search_option_value,
            'total_record' => $this->total_record,
            'page' => $this->page,
            'page_size' => $this->page_size,
            'prefix_link' => $this->prefix_link
        ];
    }
}