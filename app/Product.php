<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attribute;
use DB;

class Product extends Model
{
    public function getUrl() {
        return route('web.products.show',$this->slug);
    }
    public function getImage($size = '') {
    	// $size: tiny 80, small 150, medium 300, large 600
        if($this->image != '') {
            return image_by_link($this->image,$size);
        }else
            return '/assets/img/no-image.png';
    }
    public function getName()
    {
        return $this->name;
    }
    public function getDes($size)
    {
        return cutString(removeHTML($this->detail),$size);
    }

    public function getPrice(){
        if($this->price == 0){
            return 'Liên hệ';
        }else{
            return number_format($this->price,0,'.','.').'₫';
        }
    }
    public function getPriceOld(){
        if($this->price_old > 0){
            return number_format($this->price_old,0,'.','.').'₫';
        }else{
            return '';
        }
    }
    
    public function get_related_product() {
        return Product::where('status',1)->whereIn('id',explode(',', $this->related_products))->get();
    }

    public function attribute() {
        return $this->hasMany('App\Attribute','product_id');
    }

    public function attribute_value() {
        return DB::table('attributes')->where('product_id', $this->id)->list('key','value');
    }

    public function get_attribute($key) {
        $attribute =  $this->attribute()->where('key', $key)->first();
        if ($attribute)
            return $attribute->value;
        else
            return null;
    }

    public function set_attribute($key, $value) {
        $attribute = $this->attribute()->where('key', $key)->first();
        if (!$attribute) {
            $attribute = new Attribute;
            $attribute->product_id = $this->id;
            $attribute->key = $key;
        }
        $attribute->value = $value;
        $attribute->save();
    }
}
