<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public function getUrl() {
        return route('web.product_categories.show',$this->slug);
    }
    public function getImage($size = '') {
    	// $size: tiny 80, small 150, medium 300, large 600
        if($this->image != '') {
            return image_by_link($this->image,$size);
        }else
            return '/assets/img/no-image.png';
    }
    public function getName()
    {
        return $this->name;
    }
    public function getDes($size)
    {
        return cutString(removeHTML($this->detail),$size);
    }
}
