<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFilter extends Model
{
    public $incrementing = false;
    public $timestamp = false;
    protected $fillable =['product_id','filter_id'];
    protected $table = 'product_filters';
}
