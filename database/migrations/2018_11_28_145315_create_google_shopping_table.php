<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleShoppingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_shopping', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('type', 45);
            $table->bigInteger('type_id');
            $table->string('brand',191)->nullable();
            $table->string('category',191)->nullable();
            $table->string('instock',191)->nullable();
            $table->string('itemcondition',191)->nullable();
            $table->unique('id','id_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_shopping');
    }
}
