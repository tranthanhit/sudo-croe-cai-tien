<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFilterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('filter_id')->default(0);
            $table->string('images',191)->nullable();
            $table->string('name',191);//Tên option. Vd: 10 - 20 người
            $table->string('slug',191);//Slug của option. Vd: danh-cho-10-20-nguoi
            $table->integer('order')->default(9999);
            $table->tinyInteger('status')->default(1);
            $table->unique('id','id_UNIQUE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_details');
    }
}
