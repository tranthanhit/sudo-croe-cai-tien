<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('type_id')->default(0)->nullable();
            $table->string('type')->nullable();
            $table->tinyInteger('location_id')->default(0)->nullable();
            $table->integer('price')->default(0)->nullable();
            $table->text('name')->nullable();
            $table->tinyInteger('gender')->default(1)->nullable();
            $table->text('phone')->nullable();
            $table->text('email')->nullable();
            $table->text('address')->nullable();
            $table->text('note')->nullable();
            $table->string('link')->nullable();
            $table->integer('qty')->default(0)->nullable();
            $table->integer('status')->default(1);
         

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supports');
    }
}
