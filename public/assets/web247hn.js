/**
 * đăng nhập
 */
function login(){
    var email = $("input[name='email_login']").val();
    var password = $("input[name='password_login']").val();
   
    if(email ==''){
        alert('Bắt buộc nhập Email','error');
        $('#email_login').focus();
        return false;
    }
    if(!isEmail(email)){
        alert('Nhập sai định dạng email','error');
        $('#email_login').focus();
        return false;
    }
    if(password.length <6){
        alert('Mật khẩu không được ít hơn 6 ký tự','error');
        $('#password_login').focus();
        return false;
    }
    $.ajax({
        type: 'post',
        dataType: 'json',
        dataType:"json",      
        async: false,
        processData: false,
        contentType: false,
        url: '/ajax/register',
        data :new FormData($('#form_login')[0]),
        url: '/ajax/login',
        success:function(data){
           
        },
         error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
    
}
/**
 * đăng ký
 */
function register(){
    var name = $("input[name='name_registration']").val();
    var surname = $("input[name='surname_registration']").val();
    var email = $("input[name='email_registration']").val();
    var password = $("input[name='password_registration']").val();
   if(name ==''){
        alert('Bắt buộc nhập Tên','error');
       return false;
   }
   if(surname ==''){
        alert('Bắt buộc nhập Họ','error');
        return false;
    }
    if(email ==''){
        alert('Bắt buộc nhập Email','error');
        return false;
    }
    if(!isEmail(email)){
        alert('Nhập sai định dạng email','error');
        return false;
    }
    if(password.length <6){
        alert('Mật khẩu không được ít hơn 6 ký tự','error');
        return false;
    }
    $.ajax({
        type: 'post',
        dataType:"json",      
        async: false,
        processData: false,
        contentType: false,
        url: '/ajax/register',
        data :new FormData($('#form_registration')[0]),
        success:function(data){
            
        },
        error: function (data) {
            
        }
    });
  
}

/**
 * Thêm vào giỏi hàng
 */
$('.add_to_cart').on("click",function(event){
    event.preventDefault();
    var id = $(this).data('id');
    var type = $(this).data('type');
    var qty = 1;
    var token = $(this).data('token');
    var link =  window.location.href;
    $.ajax({
        type:"post",
        dataType:"json",
        url:"/ajax/dat-hang",
        data:{
            _token: token,
            id:id,
            type:type,
            qty :qty,
            link:link,
        },
        success:function(response){
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            $("#fancy_data").find(".layer_cart_img img").attr("src",response.image);
            $("#fancy_data").find(".product_name").html(response.name);
            $("#fancy_data").find(".ajax_cart_quantity").html(response.cart_count);
            $("#fancy_data").find(".ajax_block_cart_total").html(response.cart_total);
            $("#fancy_data").find(".layer_cart_product_quantity").html(response.qty);
            $("#fancy_data").find(".layer_cart_product_price").html(response.price);
            $(".cart .total_cart").html(response.cart_count);
            $.fancybox({
                maxWidth:455,
                minWidth:450,
                minHeight:210,
                href: "#fancy_data",
                helpers:  {
                    title : {
                        type : 'inside'
                    },
                    overlay : {
                        showEarly : false
                    },
                    autohide : 1
                }
            });
        },
        error: function (error) {
            $('img.loading1').css('display','none');
            $('body').css({'opacity':1});
            alert('Có lỗi xảy ra! Vui lòng thử lại.');
        }, beforeSend:function(){
            $('img.loading1').css('display','inline');
            $('body').css({'opacity':0.5});
        }
    });
});
