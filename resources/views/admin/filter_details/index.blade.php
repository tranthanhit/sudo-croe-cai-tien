<td>
    <strong><a class="row-title" href="{!! route($table.'.edit', $value->id) !!}" title="Sửa bài: {!! $value->name !!}">{!! $value->name !!}</a> </strong>
</td>
<td>{!! $value->slug !!}</td>
<td>
    {{$array_filters[$value->filter_id]??'Không xác định'}}
</td>
<td>
    <strong>{{__('Thêm lúc')}}:</strong> {!! $value->created_at !!}
    <br>
    <strong>{{__('Cập nhật')}}:</strong> {!! $value->updated_at !!}
</td>
<td>
    <input style="width: 100px;margin: auto;" type="text" name="order" class="form-control quick-edit" onkeyup="check_edit(this)" value="{!! $value->order !!}">
</td>