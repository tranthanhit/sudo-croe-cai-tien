{{-- 
	@include('admin.layouts.form.small_text',[
		'name' => 'text',
		'value' => 'text',
		'title' => '',
		'placeholder' => '',
		'required' => 1,
		'extra' => '',
		'slug' => 1,
		'slugField' => '',
	])
 --}}
<div class="form-group">
    <label class="control-label col-md-2 col-sm-2 col-xs-12">@if($required==1)<span class="form-asterick">* </span>@endif {!! $title??'' !!}</label>
    <div class="controls col-md-3 col-sm-10 col-xs-12">
      	<input class="form-control @if($slug == 1) {!! 'slug-title' !!} @endif" type="text" name="{!! $name??'' !!}" id="{!! $name??'' !!}" value="{!! $value??'' !!}" placeholder="{!! $placeholder??'' !!}" {!! $extra??'' !!} @if($slug == 1) data-slug="{!! $slugField??'' !!}" @endif>
    </div>
</div>