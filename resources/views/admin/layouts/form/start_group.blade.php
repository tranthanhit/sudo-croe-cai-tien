{{-- 
	@include('admin.layouts.form.start_group',[
		'name' => 'text',
		'id' => 'text',
		'in' => true,
	])
 --}}
<div class="alert alert-info row" data-toggle="collapse" data-target="#{!!$id??''!!}" style="cursor: pointer">
	<div class="col-md-2 col-sm-2 col-xs-12 text-right"><b>{!!$name??''!!}</b></div>
</div>
<div id="{!!$id??''!!}" class="collapse @if(isset($in) && $in == true) in @endif">