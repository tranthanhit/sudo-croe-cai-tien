@include('admin.layouts.setting.components.home.logo',['title' => 'Logo','name' => 'logo'])
@include('admin.layouts.setting.components.home.brands',['title' => 'Thương hiệu','name' => 'brands'])
@include('admin.layouts.setting.components.home.slideshow',['title' => 'Slideshow','name' => 'slideshow'])
@include('admin.layouts.setting.components.home.sale',['title' => 'Baner sale đầu trang','name' => 'sale'])
@include('admin.layouts.setting.components.home.comment',['title' => 'Bình luận','name' => 'comment'])
@include('admin.layouts.setting.components.home.video',['title' => 'Video  cuối trang','name' => 'video'])
@include('admin.layouts.setting.components.home.banersale',['title' => 'Baner cuối trang','name' => 'baner_bottom'])
@include('admin.layouts.setting.components.home.slogan',['title' => 'Slogan','name' => 'slogan'])
@include('admin.layouts.setting.components.home.slogan_product',['title' => 'Slogan trang chi tiết sản phẩm','name' => 'slogan_product'])
@include('admin.layouts.setting.components.home.mxh',['title' => 'facebook , youtube footer','name' => 'mxh'])
<script>
	$(document).ready(function() {
		$('.module_table_option').on('click','.delete_option',function(e) {
	        e.preventDefault();
	        $(this).closest('tr').remove();
	    });
	});
	window.addEventListener('DOMContentLoaded', (event) => {
		$('.module_table_option tbody').sortable();
	});
</script>