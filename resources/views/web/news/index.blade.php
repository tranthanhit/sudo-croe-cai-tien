@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "{{config('app.name')}}",
        "url": "{{config('app.url')}}"
    }, {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "{{config('app.url')}}",
        "@id": "{{config('app.url')}}/#organization",
        "name": "web247hn",
        "logo": "{{config('app.url')}}/public/assets/img/logo.png"
    }
</script>
@endsection
@section('content')
    Welcome !
@endsection