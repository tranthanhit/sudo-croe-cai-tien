@extends('web.layouts.app')
@section('head')
<script type="application/ld+json">
    {
        "@graph": [{
                "@context": "http://schema.org/",
                "@type": "Product",
                "sku": "{{$product->id}}",
                "id": "{{$product->id}}",
                "mpn": "MobileCity",
                "name": "{{$product->name}}",
                "description": "{{$meta_seo['description'] == '' ? cutString(removeHTML($product->detail),150) : $meta_seo['description']}}",
                "image": "{{$product->getImage()}}",
                "brand": "{{$product_category->name}}",
                "aggregateRating": {
                    "@type": "AggregateRating",
                    "ratingValue": "{{@$rank_peren}}",
                    "reviewCount": "{{(@$rank_count==0)?1:@$rank_count}}"
                },
                "review": {
                    "@type": "Review",
                    "author": "Tran Le Hai",
                    "reviewRating": {
                        "@type": "Rating",
                        "bestRating": "5",
                        "ratingValue": "1",
                        "worstRating": "1"
                    }
                },
                "offers": {
                    "@type": "AggregateOffer",
                    "priceCurrency": "VND",
                    "offerCount": 10,
                    "price": "{{(isset($product->price))?($product->price):0}}",
                    "lowPrice":"{{(isset($product->price))?($product->price):0}}",
                    "highPrice":"{{(isset($product->price_old))?$product->price_old:0}}",
                    "priceValidUntil": "2019-12-31",
                    "availability": "http://schema.org/InStock",
                    "warranty": {
                        "@type": "WarrantyPromise",
                        "durationOfWarranty": {
                            "@type": "QuantitativeValue",
                            "value": "6 tháng",
                            "unitCode": "ANN"
                        }
                    },
                    "itemCondition": "mới",
                    "seller": {
                        "@type": "Organization",
                        "name": "web247hn"
                    }
                }
            },
            {
                "@context": "http://schema.org",
                "@type": "WebSite",
                "name": "web247hn",
                "url": "{{config('app.url')}}"
            }
        ]
    }
    </script>
@endsection
@section('content')
    Welcome !
@endsection