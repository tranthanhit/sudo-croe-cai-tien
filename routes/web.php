<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * ajax
 */

Route::group(['prefix' => 'ajax'], function() {
	Route::post('register','Web\AjaxController@AjaxRegister')->name('web.ajax.register');
	Route::post('login','Web\AjaxController@AjaxLogin')->name('web.ajax.login');

	/**
	 * dat hang
	 */
	Route::post('dat-hang','Web\AjaxController@Order')->name('web.ajax.order');
});

Route::get('datafeeds','Web\HomeController@datafeeds');

Route::get('/', 'Web\HomeController@index')->name('web.home');
Route::get('/test', 'Web\TestController@index')->name('web.test');

Route::get('tim-kiem','Web\SearchController@index')->name('web.search.index');

Route::get('page-{slug}.html','Web\PageController@show')->name('web.pages.show');

Route::get('lien-he','Web\ContactController@index')->name('web.contact.index');

Route::get('tin-tuc-{slug}.html','Web\NewsController@show')->name('web.news.show');
Route::get('tin-tuc-{slug}','Web\NewsController@index')->name('web.news_categories.show');
Route::get('tin-tuc','Web\NewsController@AllShow')->name('web.news_categories.Allshow');

Route::get('{slug}.html','Web\ProductController@show')->name('web.products.show');
Route::get('san-pham','Web\ProductController@AllShow')->name('web.product_categories.allshow');
Route::get('san-pham-{slug}','Web\ProductController@index')->name('web.product_categories.show');
